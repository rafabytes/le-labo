jQuery(document).ready(function($) {

  //wow
  wow = new WOW({
    offset:50
  })
  wow.init();

  //fancybox
  $("[data-fancybox]").fancybox({
    buttons: [
    "close"
    ]
  });

  //start owl
  var rootClassName = 'owl-container';
  var navContainerClassName = '.navigation-arrows';
  var dotsContainerClassName = '.navigation-dots';
  $('.owl-carousel').each( function (index, item) {
    var items = $(this).data('items');
    var autoplay = $(this).data('autoplay');
    var paddingSm = $(this).data('padding-sm');
    var padding = $(this).data('padding');
    var loop = false;
    var mouseDrag = true;
    if( autoplay > 0 ) { loop = true; mouseDrag = false}
    var rootIdClassName = 'owl--' + rootClassName + '--' + index;
    $(item).closest('.' + rootClassName).addClass(rootIdClassName);
    $(item).owlCarousel({
      items:1,
      autoplay:autoplay,
      loop:loop,
      margin:30,
      nav:true,
      mouseDrag: mouseDrag,
      stagePadding: paddingSm,
      smartSpeed: 800,
      navContainer: '.' + rootIdClassName + ' ' +navContainerClassName,
      dotsContainer: '.' + rootIdClassName + ' ' +dotsContainerClassName,
      responsive: {
        992 : {
          items:items,
          stagePadding: padding,
          slideBy: items
        }
      }
    });
  });

  $('#owlGaleria').on('initialized.owl.carousel changed.owl.carousel', function(e) {
    if (!e.namespace)  {
      return;
    }
    var carousel = e.relatedTarget;
    $('.slider-counter').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
  });

  $('.container-fluid.relative').each(function(){
    var row = $(this).find('.row');
    var half = $(this).find('.right-half');
    if (  row.height() < half.height() ) {
      $(row).height($(half).height());
    }
  });

  if ( $('#gmaps').length ) {
    var lat = $('#gmaps').data('lat');
    var lng = $('#gmaps').data('lng');
    var endereco = {lat: lat, lng: lng};

    var map = new google.maps.Map(
      document.getElementById('gmaps'), {
        zoom: 15,
        center: endereco,
        streetViewControl : false,
        mapTypeControl: false,
        styles: [
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
          {
            "color": "#75cff0"
          }
          ]
        }
        ]
      });

    var infowindow =  new google.maps.InfoWindow();
    var marker = new google.maps.Marker({position: endereco, map: map, icon: stylesheet_directory_uri + '/images/icn-pin.svg'});
    bindInfoWindow(marker, map, infowindow, "Le Labo");

    function bindInfoWindow(marker, map, infowindow, name) {
      marker.addListener('click', function() {
        infowindow.setContent(name);
        infowindow.open(map, this);
      });
    }

  }

  $('.menu-toggle').click(function(){
    $('.menu-mobile').toggleClass('open');
  });
  $('.menu-mobile a').click(function(){
    $('.menu-mobile').removeClass('open');
  });

  $("input[type='tel']")
  .mask("(99) ?99999-9999")
  .focusout(function (event) {  
    var target, phone, element;  
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
    phone = target.value.replace(/\D/g, '');
    element = $(target);  
    element.unmask();  
    console.log(phone.length);
    if(phone.length > 10) {  
      element.mask("(99) 99999-999?9");  
    } else {  
      element.mask("(99) 9999-9999?9");  
    }  
  });

});
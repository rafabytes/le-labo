<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Home */
get_header();
?>
<a class="d-none d-md-block" id="interesse" href="#contato">Tenho Interesse</a>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section id="hero" style="background-image: url(<?php the_field('imagem_1') ?>);" class="d-flex align-items-stretch">
			<div class="col">
				<div class="line line-01 wow">
					<svg width="121" height="387" viewBox="0 0 121 387" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M120 387L120 1.00002L-234 0.99999" stroke="#FFDC82" stroke-width="2"/>
					</svg>
				</div>
				<div class="line line-02 wow">
					<svg width="355" height="194" viewBox="0 0 355 194" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 0V193H355" stroke="#43B0B7" stroke-width="2"/>
					</svg>
				</div>
				<div class="line line-03 wow">
					<svg width="210" height="188" viewBox="0 0 210 188" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M210 1L1 1L1 355" stroke="#FFDC82" stroke-width="2"/>
					</svg>
				</div>
			</div>
			<div class="container align-self-center text-center text-md-left animated fadeInUp">
				<h2><?php the_field('titulo_1') ?></h2>
				<p><?php the_field('texto_1') ?></p>
				<a href="#contato" class="interesse d-md-none">Tenho Interesse</a>
			</div>
			<div class="col">
				<div class="line line-04 wow">
					<svg width="109" height="223" viewBox="0 0 109 223" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 0V222H217" stroke="#D57284" stroke-width="2"/>
					</svg>
				</div>
				<div class="line line-05 wow">
					<svg width="279" height="210" viewBox="0 0 279 210" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M354 210L354 1.00006L1.82714e-05 1.00003" stroke="#43B0B7" stroke-width="2"/>
					</svg>
				</div>
				<div class="line line-06 wow d-none d-md-block">
					<svg width="355" height="210" viewBox="0 0 355 210" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M354 210L354 1.00006L1.82714e-05 1.00003" stroke="#FFDC82" stroke-width="2"/>
					</svg>
				</div>
				<div class="line line-07 wow d-none d-md-block">
					<svg width="217" height="168" viewBox="0 0 217 168" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 0V222H217" stroke="#D57284" stroke-width="2"/>
					</svg>
				</div>
			</div>
			<a href="#projeto" class="scroll"></a>
		</section>

		<section id="bordas" class="container-fluid">
			<div class="row">
				<div class="col-4 blue"></div>
				<div class="col-4 pink"></div>
				<div class="col-4 yellow"></div>
			</div>
		</section>

		<section id="projeto">
			<img class="wow fadeInUp d-md-none" src="<?php the_field('imagem_2') ?>" alt="Le Labo">
			<div class="container-fluid relative">
				<div class="line line-08 wow d-none d-md-block">
					<svg width="66" height="217" viewBox="0 0 66 217" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M66 1L-156 0.99999L-156 217" stroke="#D57284" stroke-width="2"/>
					</svg>
				</div>
				<div class="container lg">
					<div class="row">
						<div class="col-md-6">
							<div class="wow fadeInLeft text-center text-md-left">
								<h2 class="blue"><?php the_field('titulo_2') ?></h2>
								<?php the_field('texto_2') ?>
							</div>
						</div>
						<div class="col-md-6 right-half d-none d-md-flex">
							<div class="line line-09 wow">
								<svg width="355" height="210" viewBox="0 0 355 210" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M354 210L354 1L1.82714e-05 0.999969" stroke="#FFDC82" stroke-width="2"/>
								</svg>
							</div>
							<img class="wow fadeInRight" src="<?php the_field('imagem_2') ?>" alt="Le Labo">
							<div class="line line-10 wow">
								<svg width="210" height="135" viewBox="0 0 210 135" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M210 1L1.00001 1L1.00001 135" stroke="#43B0B7" stroke-width="2"/>
								</svg>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="diferenciais">
			<div class="d-flex align-items-stretch">
				<div class="col">
					<div class="line line-11 wow">
						<svg width="191" height="485" viewBox="0 0 191 485" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M191 1L-18 1L-18 485" stroke="#43B0B7" stroke-width="2"/>
						</svg>
					</div>
					<div class="line line-12 wow d-none d-md-block">
						<svg width="95" height="340" viewBox="0 0 95 340" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M94 340L94 1.00004L-260 1.00001" stroke="#FFDC82" stroke-width="2"/>
						</svg>
					</div>
					<div class="line line-13 wow">
						<svg width="185" height="75" viewBox="0 0 185 75" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 0V74H185" stroke="#D57284" stroke-width="2"/>
						</svg>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="wow fadeInUp text-center text-md-left">
								<h2 class="pink"><?php the_field('titulo_11') ?></h2>
								<p><?php the_field('texto_11') ?></p>
							</div>
						</div>
						<div class="col-6 col-md-5 d-none d-md-flex">
							<div class="wow fadeInLeft">
								<div class="item elevadores"><span class="icone" style="background-image: url(<?php the_field('icone_1') ?>);"></span><?php the_field('item_1') ?></div>
								<div class="item biometria"><span class="icone" style="background-image: url(<?php the_field('icone_2') ?>);"></span><?php the_field('item_2') ?></div>
								<div class="item piso"><span class="icone" style="background-image: url(<?php the_field('icone_3') ?>);"></span><?php the_field('item_3') ?></div>
								<div class="item rooftop"><span class="icone" style="background-image: url(<?php the_field('icone_4') ?>);"></span><?php the_field('item_4') ?></div>
							</div>
						</div>
						<div class="col-6 col-md-7 d-none d-md-flex">
							<div class="wow fadeInRight">
								<div class="item reunioes"><span class="icone" style="background-image: url(<?php the_field('icone_5') ?>);"></span><?php the_field('item_5') ?></div>
								<div class="item carro"><span class="icone" style="background-image: url(<?php the_field('icone_6') ?>);"></span><?php the_field('item_6') ?></div>
								<div class="item alimentacao"><span class="icone" style="background-image: url(<?php the_field('icone_7') ?>);"></span><?php the_field('item_7') ?></div>
								<div class="item vestuario"><span class="icone" style="background-image: url(<?php the_field('icone_8') ?>);"></span><?php the_field('item_8') ?></div>
							</div>
						</div>
						<div class="col-6 col-md-5 d-md-none">
							<div class="wow fadeInLeft">
								<div class="item elevadores"><span class="icone" style="background-image: url(<?php the_field('icone_1') ?>);"></span><?php the_field('item_1') ?></div>
								<div class="item piso"><span class="icone" style="background-image: url(<?php the_field('icone_3') ?>);"></span><?php the_field('item_3') ?></div>
								<div class="item reunioes"><span class="icone" style="background-image: url(<?php the_field('icone_5') ?>);"></span><?php the_field('item_5') ?></div>
								<div class="item alimentacao"><span class="icone" style="background-image: url(<?php the_field('icone_7') ?>);"></span><?php the_field('item_7') ?></div>
							</div>
						</div>
						<div class="col-6 col-md-7 d-md-none">
							<div class="wow fadeInRight">
								<div class="item biometria"><span class="icone" style="background-image: url(<?php the_field('icone_2') ?>);"></span><?php the_field('item_2') ?></div>
								<div class="item rooftop"><span class="icone" style="background-image: url(<?php the_field('icone_4') ?>);"></span><?php the_field('item_4') ?></div>
								<div class="item carro"><span class="icone" style="background-image: url(<?php the_field('icone_5') ?>);"></span><?php the_field('item_6') ?></div>
								<div class="item vestuario"><span class="icone" style="background-image: url(<?php the_field('icone_8') ?>);"></span><?php the_field('item_8') ?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="line line-14 wow">
						<svg width="355" height="210" viewBox="0 0 355 210" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M354 210L354 1L1.82714e-05 0.999969" stroke="#FFDC82" stroke-width="2"/>
						</svg>
					</div>
					<div class="line line-15 wow">
						<svg width="121" height="223" viewBox="0 0 121 223" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 0V222H217" stroke="#D57284" stroke-width="2"/>
						</svg>
					</div>
				</div>
			</div>
		</section>

		<section id="galeria" class="bg-blue">
			<div class="container lg">
				<div class="row align-items-end text-center text-md-left">
					<div class="col-md-5">
						<h1 class="super outline wow fadeInLeft"><?php the_field('titulo_3') ?></h1>
					</div>
					<div class="col-md-7">
						<p class="wow fadeInRight"><?php the_field('texto_3') ?></p>
					</div>
				</div>
			</div>
			<?php 
			$images = acf_photo_gallery( 'galeria_1' , get_the_ID() );
			if ( is_array($images) || is_object($images) ) : ?>
				<div class="owl-container">
					<div class="container lg">
						<div id="owlGaleria" class="owl-gallery owl-carousel wow slideInRight" data-items="1" data-padding="0">
							<?php foreach( $images as $image ): ?>
								<div class="item">
									<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="fancyGaleria" data-caption="LeLabo">
										<div class="img" style="background-image: url(<?php echo $image['full_image_url'] ?>);"></div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row align-items-center">
							<div class="col-md-7">
								<div class="navigation-dots d-none d-md-flex"></div>
							</div>
							<div class="col-md-5 text-right">
								<a href="<?php echo $images[0]['full_image_url'] ?>" class="fullscreen" data-fancybox="fancyGaleria">Tela Cheia</a>
								<div class="slider-counter">1/4</div>
								<div class="navigation-arrows"></div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>

		<section id="ideias">
			<div class="container lg">
				<div class="row">
					<div class="col-md-12">
						<div class="wow fadeInRight">
							<h6><?php the_field('subtitulo_4') ?></h6>
							<h2><?php the_field('titulo_4') ?></h2>
							<p><?php the_field('texto_4') ?></p>
						</div>
					</div>
				</div>
			</div>
			<?php 
			$images = acf_photo_gallery( 'galeria_2' , get_the_ID() );
			if ( is_array($images) || is_object($images) ) : ?>
				<div class="owl-container">
					<div class="container lg">
						<div id="owlIdeias" class="owl-gallery owl-carousel wow slideInRight" data-items="2" data-padding-sm="30" data-padding="100">
							<?php foreach( $images as $image ): ?>
								<div class="item">
									<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="fancyIdeias" data-caption="LeLabo">
										<div class="img" style="background-image: url(<?php echo $image['full_image_url'] ?>);"></div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="container lg">
						<div class="row align-items-center">
							<div class="col-6 col-md-7">
								<div class="navigation-dots"></div>
							</div>
							<div class="col-6 col-md-5 text-right">
								<div class="navigation-arrows"></div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>

		<section id="formula" class="bg-yellow">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="wow fadeInUp">
							<h2><?php the_field('titulo_5') ?></h2>
							<p><?php the_field('texto_5') ?></p>
						</div>
					</div>
				</div>
			</div>
			<?php 
			$images = acf_photo_gallery( 'galeria_3' , get_the_ID() );
			if ( is_array($images) || is_object($images) ) : ?>
				<div class="owl-container">
					<div class="container lg">
						<div id="owlFormula" class="owl-gallery owl-carousel wow slideInRight" data-items="2" data-padding-sm="30" data-padding="100">
							<?php foreach( $images as $image ): ?>
								<div class="item">
									<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="fancyFormula" data-caption="LeLabo">
										<div class="img" style="background-image: url(<?php echo $image['full_image_url'] ?>);"></div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="container lg">
						<div class="row align-items-center">
							<div class="col-6 col-md-7">
								<div class="navigation-dots"></div>
							</div>
							<div class="col-6 col-md-5 text-right">
								<div class="navigation-arrows"></div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>

		<section id="localizacao">
			<div class="line line-16 wow">
				<svg width="355" height="210" viewBox="0 0 355 210" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M354 210L354 1L1.82714e-05 0.999969" stroke="#FFDC82" stroke-width="2"/>
				</svg>
			</div>
			<div class="line line-17 wow">
				<svg width="143" height="223" viewBox="0 0 143 223" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1 0V222H217" stroke="#D57284" stroke-width="2"/>
				</svg>

			</div>
			<div class="line line-18 wow d-none d-md-block">
				<svg width="210" height="355" viewBox="0 0 210 355" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M210 1L1 1L1 355" stroke="#FFDC82" stroke-width="2"/>
				</svg>
			</div>
			<div class="line line-19 wow">
				<svg width="116" height="194" viewBox="0 0 116 194" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1 0V193H116" stroke="#43B0B7" stroke-width="2"/>
				</svg>
			</div>
			<div class="container lg">
				<div class="row">
					<div class="col-md-12">
						<div class="wow fadeInUp">
							<h6><?php the_field('subtitulo_6') ?></h6>
							<h2><?php the_field('titulo_6') ?></h2>
							<p><?php the_field('texto_6') ?></p>
						</div>
					</div>
				</div>
			</div>
			<div id="gmaps" data-lat="-7.1464332" data-lng="-34.9516375"></div>
		</section>

		<section id="obra" class="bg-blue">
			<div class="line line-20 wow d-none d-md-block">
				<svg width="251" height="223" viewBox="0 0 251 223" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1 0V222H325" stroke="#D57284" stroke-width="2"/>
				</svg>
			</div>
			<div class="line line-21 wow d-none d-md-block">
				<svg width="147" height="216" viewBox="0 0 147 216" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M-79 215L146 215L146 -1.69876e-05" stroke="#FFDC82" stroke-width="2"/>
				</svg>
			</div>
			<div class="line line-22 wow d-none d-md-block">
				<svg width="216" height="98" viewBox="0 0 216 98" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1 0V225H216" stroke="#D57284" stroke-width="2"/>
				</svg>
			</div>
			<div class="container lg">
				<img class="wow fadeInUp" src="<?php the_field('imagem_7') ?>">
				<div class="row">
					<div class="offset-md-3 col-md-9">
						<div class="wow fadeInRight text-center text-md-left">
							<h6><?php the_field('subtitulo_7') ?></h6>
							<h1 class="outline"><?php the_field('titulo_7') ?></h1>
							<a href="<?php the_field('botao_link_7') ?>"><?php the_field('botao_7') ?></a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="contato" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/lelabo-footer.jpg);">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-md-6 cta">
						<div class="container sm">
							<div class="wow fadeInUp">
								<h6><?php the_field('subtitulo_8') ?></h6>
								<h2><?php the_field('titulo_8') ?></h2>
								<p><?php the_field('texto_8') ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-6 form bg-blue">
						<div class="container sm">
							<div class="text-center text-md-left wow fadeInUp">
								<h2><?php the_field('titulo_9') ?></h2>
								<p><?php the_field('texto_9') ?></p>
							</div>
							<div class="wow fadeInUp">
								<?php echo do_shortcode( '[contact-form-7 id="6" title="Contato"]' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</main>
</div>

<?php
get_footer();

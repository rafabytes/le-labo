<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

	<script type="text/javascript">
		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
	</script>

	<meta name="language" content="pt-BR">
	<meta name="robots" content="all">
	<meta name="author" content="Qualitare">
	<meta name="keywords" content="empreendimento, coworking, trabalho em equipe, experimentar, startups, joão pessoa">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">

		<header id="masthead" class="site-header animated fadeIn">
			<div class="container lg d-none d-md-flex justify-content-between align-items-center">
				<div class="">
					<a href="<?php echo home_url( '#projeto' ) ?>">O Projeto</a>
					<a href="<?php echo home_url( '#diferenciais' ) ?>">Diferenciais</a>
				</div>
				<div><a href="<?php echo home_url( '' ) ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Le Labo"></a></div>
				<div class="">
					<a href="<?php echo home_url( '#galeria' ) ?>">Galeria</a>
					<a href="<?php echo home_url( '#localizacao' ) ?>">Localização</a>
				</div>
			</div>
			<div class="menu-mobile d-md-none">
				<img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Le Labo">
				<div class="menu-toggle">
					<span></span>
				</div>
				<ul>
					<li><a href="<?php echo home_url( '#projeto' ) ?>">O Projeto</a></li>
					<li><a href="<?php echo home_url( '#diferenciais' ) ?>">Diferenciais</a></li>
					<li><a href="<?php echo home_url( '#galeria' ) ?>">Galeria</a></li>
					<li><a href="<?php echo home_url( '#localizacao' ) ?>">Localização</a></li>
				</ul>
			</div>
		</header><!-- #masthead -->

		<div id="content" class="site-content">

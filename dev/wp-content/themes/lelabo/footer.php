<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */
$homepage = get_page_by_path( 'homepage' );
$texto = get_field('texto_10',$homepage->ID);
$telefone = get_field('telefone',$homepage->ID);
$celular = get_field('celular',$homepage->ID);
$facebook = get_field('facebook',$homepage->ID);
$facebook_link = get_field('facebook_link',$homepage->ID);
$instagram = get_field('instagram',$homepage->ID);
$instagram_link = get_field('instagram_link',$homepage->ID);
// $privacidade = get_page_by_path( 'homepage' );
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer text-center text-md-left">
	<div class="line line-23 wow">
		<svg width="79" height="216" viewBox="0 0 79 216" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M-147 215L78 215L78 -1.69876e-05" stroke="#FFDC82" stroke-width="2"/>
		</svg>
	</div>
	<div class="line line-24 wow d-none d-md-block">
		<svg width="216" height="93" viewBox="0 0 216 93" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M1 0V225H216" stroke="#41A9B0" stroke-width="2"/>
		</svg>
	</div>
	<div class="line line-25 wow d-none d-md-block">
		<svg width="326" height="210" viewBox="0 0 326 210" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M354 210L354 1L1.82714e-05 0.999969" stroke="#FFDC82" stroke-width="2"/>
		</svg>
	</div>
	<div class="line line-26 wow d-none d-md-block">
		<svg width="92" height="223" viewBox="0 0 92 223" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M1 0V222H218" stroke="#D57284" stroke-width="2"/>
		</svg>
	</div>
	<div class="line line-27 wow d-none d-md-block">
		<svg width="226" height="136" viewBox="0 0 226 136" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M0 215L225 215L225 -1.69876e-05" stroke="#41A8AF" stroke-width="2"/>
		</svg>
	</div>
	<div class="line line-28 wow d-md-none">
		<svg width="116" height="194" viewBox="0 0 116 194" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M1 0V193H116" stroke="#43B0B7" stroke-width="2"/>
		</svg>
	</div>
	<div class="site-info">
		<div class="container lg">
			<div class="row">
				<div class="col-md-5">
					<div class="wow fadeInLeft">
						<img class="logo" src="<?php echo get_template_directory_uri() ?>/images/logo-footer.svg" alt="Le Labo">
						<p><?php echo $texto; ?></p>
					</div>
				</div>
				<div class="col-md-6 offset-md-1">
					<div class="wow fadeInRight">
						<h4>Central de vendas</h4>
						<div class="fones row mr-0 ml-0">
							<div class="col-md-6 pl-0">
								<div class="fone"><a href="tel:<?php echo $telefone; ?>"><?php echo $telefone; ?></a></div>
							</div>
							<div class="col-md-6 pr-0">
								<div class="cel"><a href="https://api.whatsapp.com/send?phone=<?php echo $celular; ?>"><?php echo $celular; ?></a></div>
							</div>
						</div>
						<h5>Redes sociais</h5>
						<div class="social">
							<a href="<?php echo $facebook_link; ?>" class="facebook" target="_blank"><?php echo $facebook; ?></a>
							<a href="<?php echo $instagram_link; ?>" class="instagram" target="_blank"><?php echo $instagram; ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-md-5">
					<a class="privacidade" href="<?php echo home_url( '/politica-de-privacidade/' ) ?>">Política de Privacidade</a>
				</div>
				<div class="col-md-6 offset-md-1">
					<div class="copyrights">Todos os direitos reservados. Le Labo © 2021</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="http://qualitare.com/" class="qualitare d-none d-md-block" target="_blank"></a>
				</div>
			</div>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-188242777-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-188242777-2');
</script>

</body>
</html>
